global_variable = 0 #global or module level variable

class BaseClassName(): #some class defined in a module
    pass
    #it is necessary to write "pass" inside a class when the class is empty

class ClassName(BaseClassName):
    inside_class_field="xxx"
    def method(x,not_calling_from_instance=False):
        #a method is a function inside a class
        print("I'm a method of a class")
        inner_function_variable=0
        if not not_calling_from_instance: 
           x.instance_attribute=1
        for a in range(10):
            inside_loop_valriabe=a
            print(inside_loop_valriabe)
        another_inside_function_variable="outside the loop"
        print("the first argument x is",x)
        if hasattr(x,"inside_class_field"):
            #checks if x has an attribute
            print("x.inside_class_field value is",x.inside_class_field)
        #another code outside if condition and for loop
    @staticmethod
    def another_method(y):
        print(y)
        ## @staticmethod indicates that the method's first argument 
        ## is not necessary an instance of that class

def module_level_function(x):
    z=x+1
    #this function is outside the ClassName class
    y=x+2
    print("I'm inside a module_level_function","and y=",y)
    #another code inside a module level (global) function
	#tab identation is not the same as several spaces, should not be combined

def another_module_level_function(arg1,arg2):
	print("the first argument of a another_module_level_function is",arg1)
	print("the second argument of a another_module_level_function is",arg2)
	for i in range(5):
		#inside a loop
		print(i, "I'm inside a loop in an another_module_level_function")
	#when only tabs are used inside a function, it's OK

another_global_variable="xxx"

print("calling module level (global) functions:"); print()
another_module_level_function(1,"x")
module_level_function(2)
print('---'); print("calling class static method:");print()
ClassName.another_method("another_method test")
print('---'); print("initializing myclass object..."); print()
myclass=ClassName()
ClassName.method("class_level_call_of_a_method",not_calling_from_instance=True)
print('---'); print("calling a non-static method"); print()
myclass.method() #without arguments as the first argument x is a 
                 #class instance itself when no @statichmethod
print("instance_attribute is",myclass.instance_attribute)

input("Press any key to continue")

