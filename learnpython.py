# Comments start with a # symbol.

####################################################
## Basics
####################################################
## Types:

# integer
3

# float (floating-point number)
3.14

# Math is what you would expect
1 + 1  # => 2
8 - 1  # => 7
10 * 2  # => 20
35 / 5  # => 7.0

# Exponentiation (x**y, x to the yth power)
2**3  # => 8

# Enforce precedence with parentheses
1 + 3 * 2    # => 7
(1 + 3) * 2  # => 8

# Mathematical functions :
# https://docs.python.org/3/library/math.html#power-and-logarithmic-functions
import math
math.sqrt(144)  # => 12.0
math.pi # => 3.141592653589793
#or
from math import *
sqrt(144)  # => 12.0
sin(pi/2) # => 1.0
log(e**10) # => 10.0


# Variables
some_var = 5
some_var  # => 5

interm_result = (1 + 3) * 2
interm_result**3  # => 512


# More complex calculations using external modules:
## pip install sympy
from sympy import *
x = Symbol('x')
f = x**2 + 5*x + 1
deriv_f = f.diff(x)
deriv_f # 2*x + 5
deriv_f.subs(x,3) # => 11

# A string (text) is created with " or '
"This is a string."
'This is also a string.'

# Strings can be added too!
"Hello " + "world!"  # => "Hello world!"

# Get the number of characters:
s = "Hello " + "world!"
len(s) # => 12

# multiplication by an integer:
"a"*4 # => "aaaa"

# Include a variable inside a string:
some_var = 5
"The some_var value is "+str(some_var)+"."
# OR
"The some_var value is %d."%some_var
# OR
"The some_var value is {}".format(some_var)
# OR
f"The some_var value is {some_var}"


# None means an empty/nonexistent value
None  # => None

# Boolean values are primitives (Note: the capitalization)
True   # => True
False  # => False

# negate with not
not True   # => False
not False  # => True

# Boolean Operators
# Note "and" and "or" are case-sensitive
True and False  # => False
False or True   # => True

# Equality is ==
1 == 1  # => True
2 == 1  # => False

# Inequality is !=
1 != 1  # => False
2 != 1  # => True

# More comparisons
1 < 10  # => True
1 > 10  # => False
2 <= 2  # => True
2 >= 2  # => True

# String comparisons
"Hello world!" == 'Hello world!'  # => True
"Hello world!" == "hello world!"  # => False, case sensitive!
"Hello world!" != "hello world !"  # => True


####################################################
## Lists and dicts
####################################################

# Lists store sequences
li = []

# Add stuff to the end of a list with append
li.append(1)    # li is now [1]
li.append(2)    # li is now [1, 2]
li.append(3)    # li is now [1, 2, 3]

# Access a list like you would any array
# in Python, the first list index is 0, not 1.
li[0]  # => 1
# Look at the last element
li[-1]  # => 3
# Assign new values to indexes that have already been initialized with =
li[0] = 42
li # => [42, 2, 3]


# You can add lists
other_li = [4, 5, 6]
li + other_li   # => [42, 2, 3, 4, 5, 6]

# Get the length with "len()"
len(li)   # => 6

# Tuples are unmutable lists (no assign, no append):
a_tuple = (1,2,3)
# may be converted to lists:
a_list = list(a_tuple)
a_list[2] = 5
a_tuple # => (1, 2, 3)
a_list  # => [1, 2, 5]

#Dicts
empty_dict = {}
# Here is a prefilled dictionary
filled_dict = {"one": 1, "two": 2, "three": 3}
# OR
filled_dict = dict(one=1, two=2, three=3)

# Look up values with []
filled_dict["one"]  # => 1

some_person = dict(name='Lancelot', quest="To find the holy grail", favorite_color="Blue")

some_person['name']   # => 'Lancelot'

# Check for existence of keys in a dictionary with "in"
'name' in some_person   # => True
'age' in some_person   # => False

# set the value of a key with a syntax similar to lists
some_person["age"] = 30  # now, some_person["age"] => 30

# type conversion:
str(2) # => "2"
int('2') # => 2
float('3.14') # => 3.14
int(3.14) # => 3
float(3) # => 3.0

list(range(5)) # => [0, 1, 2, 3, 4]
list("abcd") # => ['a', 'b', 'c', 'd']
str(['a', 'b', 'c', 'd']) #=> "['a', 'b', 'c', 'd']"

int("2")*3 # => 6
int(str(2)*3) # => 222


# print() displays the value in your command prompt window
print("I'm Python. Nice to meet you!") # => I'm Python. Nice to meet you!
print("The result of 2 >= 2 is",2 >= 2) # => The result of 2 >= 2 is True

####################################################
##  Control Flow
####################################################

# Let's just make a variable
some_var = 5

# Here is an if statement.
# prints "some_var is smaller than 10"
if some_var > 10:
    print("some_var is totally bigger than 10.")
elif some_var < 10:    # This elif clause is optional.
    print("some_var is smaller than 10.")
else:           # This is optional too.
    print("some_var is indeed 10.")

"""
SPECIAL NOTE ABOUT INDENTING
In Python, you must indent your code correctly, or it will not work.
All lines in a block of code must be aligned along the left edge.
When you're inside a code block (e.g. "if", "for", "def"; see below),
you need to indent by the same number of spaces or tabs (usually 4 spaces, or more seldom 1 tab).
Do not mix tabs and spaces!

Examples of wrong indentation:

if some_var > 10:
print("bigger than 10." # error, this line needs to be indented by 4 spaces


if some_var > 10:
    print("bigger than 10.")
 else: # error, this line needs to be unindented by 1 space
    print("less than 10")

"""


"""
For loops iterate over lists
prints:
    1
    4
    9
"""
for x in [1, 2, 3]:
    print(x*x)

"""
"range(number)" returns a list of numbers
from zero to the given number MINUS ONE

the following code prints:
    0
    1
    2
    3
"""
for i in range(4):
    print(i)

####################################################
## List comprehensions
####################################################

# We can use list comprehensions to loop or filter
numbers = [3,4,5,6,7]
[x*x for x in numbers]  # => [9, 16, 25, 36, 49]

numbers = [3, 4, 5, 6, 7]
[x for x in numbers if x > 5]   # => [6, 7]


####################################################
## Functions
####################################################

# Use "def" to create new functions
def add(x, y):
    print('x is', x)
    print('y is', y)
    return x + y

# Calling functions with parameters
add(5, 6)   # => prints out "x is 5 and y is 6" and returns 11


####################################################
## Classes
####################################################

# We use the "class" statement to create a class
class Human:
    # A class attribute. It is shared by all instances of this class
    species = "H. sapiens"

    # Basic initializer, this is called when this class is instantiated.
    # Methods(or objects or attributes) like: __init__, __str__,
    # __repr__ etc. are called special methods. You should not invent such names on your own.
    def __init__(self, name, age=0):
        # Assign the argument to the instance's name attribute
        self.name = name
        self.age = age   # the variable "age" may not be present, it's value is 0 therefore
    
    # An instance method. All methods take "self" as the first argument
    def say(self, msg):
        print(f"{self.name}: {msg}")

    # Another instance method
    def present_yourself(self):
        phrase = "Hello, my name is "+self.name
        if self.age > 0:
            phrase += f", I'm {self.age} years old"
        phrase += "."
        print(phrase)

    # A static method is called without a class or instance reference
    @staticmethod
    def add(x,y):
        # no self in the first place
        print(f"{x}+{y} =",x+y)

i = Human(name="Ian")
i.say("hi") # => Ian: hi
i.present_yourself() # => Hello, my name is Ian.

j = Human("Joseph",24)
j.say("hello") # => Joseph: hello
j.present_yourself() # => Hello, my name is Joseph, I'm 24 years old.

i.age=22
i.present_yourself() # => Hello, my name is Ian, I'm 22 years old.

# @staticmethod:
i.add(3,5) # => 3+5 = 8
j.add(2,4) # => 2+4 = 6
Human.add(7,8) # => 7+8 = 15


####################################################
## Inheritance
####################################################

# Inheritance allows new child classes to be defined that inherit methods and
# variables from their parent class.

# Using the Human class defined above as the base or parent class, we can
# define a child class, Student, which inherits variables like "species",
# "name", and "age", as well as methods, like "say", "present_yourself" and "add"
# from the Human class, but can also have its own unique properties.



# Specify the parent class(es) as parameters to the class definition
class Student(Human):
    # If the child class should inherit all of the parent's definitions without
    # any modifications, you can just use the "pass" keyword (and nothing else)
    # but in this case it is commented out to allow for a unique child class:
    # pass

    # Child classes can override their parents' attributes
    species = "H. discipulus"

    # Children automatically inherit their parent class's constructor including
    # its arguments, but can also define additional arguments or definitions
    # and override its methods such as the class constructor.
    # This constructor inherits the "name" and "age" arguments from the "Human" class and
    # adds the "university"  argument:
    def __init__(self, name, age=0, university=""):

        # add additional class attributes:
        self.student = True
        self.university = university

        # The "super" function lets you access the parent class's methods
        # that are overridden by the child, in this case, the __init__ method.
        # This calls the parent class constructor:
        super().__init__(name,age)

    # override the present_yourself method
    def present_yourself(self):
        phrase = "Hello, my name is "+self.name
        if self.age > 0:
            phrase += f", I'm {self.age} years old"
        if self.university:
            phrase += f", I'm from {self.university} university"
        phrase += "."
        print(phrase)

i = Student("Ian",22,"Paris 1")
i.present_yourself() # => Hello, my name is Ian, I'm 22 years old, I'm from Paris 1 university.
i.say('hi') # => Ian: hi
i.add(2,3) # => 2+3 = 5



####################################################
## Modules
####################################################
# A module is a python file which contains definitions and statements. 
# Or a folder with __init__.py in it containing definitions and statements
# Python has a lot of build-in modules like math, random, csv...

# You can import modules
import random
print(random.random()) # random real between 0 and 1

# To import functions from other modules and call them without the module name
# from "module_name (filename-without-extension or folder_name)" import "function-or-class"

from random import random
#OR from random import * # to import all functions
print(random()) # random real between 0 and 1
